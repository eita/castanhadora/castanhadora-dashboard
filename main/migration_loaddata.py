# SOURCE: https://gist.github.com/danni/1b2a0078e998ac080111
import os
import logging

from django.core import serializers

LOGGER = logging.getLogger(__name__)


def load_fixture(app, fixture, ignorenonexistent=True):
    """
    A factory to load a named fixture via a data migration.
    """

    def inner(apps, schema_editor):
        """
        Loads migrations that work at current state of a model, in constrast to
        `loaddata` which requires a fixture to have data matching the fields
        defined in `models.py`.

        Based on https://gist.github.com/leifdenby/4586e350586c014c1c9a
        """

        # relative path to fixtures
        fixtures_dir = os.path.join(app, 'fixtures')

        # monkey patch serializers `apps` so that it uses the models in the
        # current migration state
        original_apps = serializers.python.apps

        try:
            serializers.python.apps = apps

            objects = None

            for extension in ('json', 'yaml', 'xml'):
                fixture_path = os.path.join(
                    fixtures_dir,
                    '%s.%s' % (fixture, extension))

                LOGGER.debug("Trying %s", fixtures_dir)

                if os.path.exists(fixture_path):
                    print("Loading fixtures from %s... " % fixture_path)

                    with open(fixture_path, 'rb') as file_:
                        objects = serializers.deserialize(
                            extension, file_,
                            ignorenonexistent=ignorenonexistent)

                        count = 0
                        for obj in objects:
                            obj.save()
                            count += 1

                        print("Loaded %d objects." % count)

            if objects is None:
                raise Exception(
                    "Couldn't find the '%s' fixture for the '%s' app." % (
                        fixture, app))
        finally:
            serializers.python.apps = original_apps

    return inner