from django.contrib import admin
import main.models as models

class UpfAdmin(admin.ModelAdmin):
    list_display = ('nome_usuario','nome_comunidade')

class SafraAdmin(admin.ModelAdmin):
    list_display = ('upf', 'nome', 'data_inicio', 'ativa', 'simulacao', 'municipio')

    def municipio(self, obj):
        return obj.get_municipio()

class DespesaAdmin(admin.ModelAdmin):
    list_display = ('upf', 'safra', 'tipo', 'valor', 'data')

    def upf(self, obj):
        return obj.safra.upf

class VendaAdmin(admin.ModelAdmin):
    list_display = ('upf', 'safra', 'data', 'quantidade', 'valor_unitario', )

    def upf(self, obj):
        return obj.safra.upf

class ProducaoAdmin(admin.ModelAdmin):
    list_display = ('upf', 'safra', 'data', 'quantidade')

    def upf(self, obj):
        return obj.safra.upf

class UpfIntegranteAdmin(admin.ModelAdmin):
    list_display = ('upf', 'safra', 'nome', 'data_nascimento', 'genero', 'parentesco', 'main_integrante')

    def upf(self, obj):
        return obj.safra.upf

class TrabalhoUpfAdmin(admin.ModelAdmin):
    list_display = ('upf', 'safra', 'data', 'dias')

    def upf(self, obj):
        return obj.safra.upf

class TrabalhoUpfIntegranteAdmin(admin.ModelAdmin):
    list_display = ('upf', 'safra', 'upf_integrante')

    def safra(self, obj):
        return obj.trabalho_upf.safra

    def upf(self, obj):
        return obj.trabalho_upf.safra.upf

class EquipamentoAdmin(admin.ModelAdmin):
    list_display = ('upf', 'safra', 'tipo', 'quantidade', 'valor_compra', 'ano_fabricacao', 'ano_compra', 'durabilidade_estimada_anos', 'usado')

    def upf(self, obj):
        return obj.safra.upf

admin.site.register(models.Upf, UpfAdmin)
admin.site.register(models.Safra, SafraAdmin)
admin.site.register(models.Despesa, DespesaAdmin)
admin.site.register(models.Venda, VendaAdmin)
admin.site.register(models.Producao, ProducaoAdmin)
admin.site.register(models.UpfIntegrante, UpfIntegranteAdmin)
admin.site.register(models.TrabalhoUpf, TrabalhoUpfAdmin)
admin.site.register(models.TrabalhoUpfIntegrante, TrabalhoUpfIntegranteAdmin)
admin.site.register(models.Equipamento, EquipamentoAdmin)
