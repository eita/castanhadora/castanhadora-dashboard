from django.db import models
from random import randint

# Create your models here.
class BaseModel(models.Model):  # base class should subclass 'django.db.BaseModel'
    mobile_key = models.CharField(max_length=150, blank=True, null=True)
    mobile_model_id = models.IntegerField(null=True, default=None)
    class Meta:
        abstract=True
        constraints = [
            models.UniqueConstraint(fields=['mobile_model_id', 'mobile_key'], name='%(class)s_unique')
        ]

class Upf(BaseModel):
    nome_comunidade = models.CharField(max_length=150, blank=True, null=True)
    nome_usuario = models.CharField(max_length=150, blank=True, null=True)
    data_nascimento_usuario = models.DateField(blank=True, null=True)
    genero_usuario = models.CharField(max_length=150, blank=True, null=True)
    unidade_area = models.CharField(max_length=150, blank=True, null=True)
    unidade_valor = models.CharField(max_length=150, blank=True, null=True)
    cooperativa = models.CharField(max_length=150, blank=True, null=True)

    @classmethod
    def gera_senha_aleatoria(self):
        """
        :return: uma string com 7 dígitos
        """
        return "{input:07d}".format(input=randint(0, 9999999))

    def __str__(self):
        nome = self.nome_usuario if self.nome_usuario else id
        if self.nome_comunidade:
            nome += "(" + nome_comunidade + ")"
        return nome

class Safra(BaseModel):
    nome = models.CharField(max_length=150, blank=True, null=True)
    data_inicio = models.DateField(blank=True, null=True)
    data_final = models.DateField(blank=True, null=True)
    ativa = models.BooleanField(blank=True, null=True)
    remuneracao_desejada_por_dia = models.FloatField(blank=True, null=True)
    castanhal_tamanho = models.FloatField(blank=True, null=True)
    castanhal_numero_castanheiras = models.IntegerField(blank=True, null=True)
    castanhal_numero_familias = models.IntegerField(blank=True, null=True)
    municipio_id = models.IntegerField(blank=True, null=True)
    uf_id = models.IntegerField(blank=True, null=True)
    simulacao = models.BooleanField(blank=True, null=True)
    localizacao_lat = models.FloatField(blank=True, null=True)
    localizacao_long = models.FloatField(blank=True, null=True)
    upf = models.ForeignKey(Upf, on_delete=models.CASCADE, null=True, default=None)

    def __str__(self):
        nome = self.nome
        return nome

    def get_municipio(self):
        municipio = Municipio.objects.get(pk=self.municipio_id)
        return municipio

class CategoriaDespesa(BaseModel):
    nome = models.CharField(max_length=150, blank=True, null=True)

class Despesa(BaseModel):
    tipo = models.CharField(max_length=150, blank=True, null=True)
    valor = models.FloatField(blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    observacoes = models.TextField(max_length=150, blank=True, null=True)
    aviamento = models.BooleanField(blank=True, null=True)
    diaria_dias = models.FloatField(blank=True, null=True)
    diaria_quantidade_diaristas = models.IntegerField(blank=True, null=True)
    diaria_valor_diaria = models.FloatField(blank=True, null=True)
    safra = models.ForeignKey(Safra, on_delete=models.CASCADE, null=True, default=None)
    # categoria_despesa = models.ForeignKey(CategoriaDespesa, on_delete=models.CASCADE, null=True, default=None)

    def __str__(self):
        return self.tipo

class Venda(BaseModel):
    quantidade = models.IntegerField(blank=True, null=True)
    valor_unitario = models.FloatField(blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    observacoes = models.TextField(max_length=150, blank=True, null=True)
    comprador_id = models.IntegerField(blank=True, null=True)
    comprador = models.CharField(max_length=150, blank=True, null=True)
    quitacao_aviamento = models.BooleanField(blank=True, null=True)
    safra = models.ForeignKey(Safra, on_delete=models.CASCADE, null=True, default=None)

class Producao(BaseModel):
    data = models.DateField(blank=True, null=True)
    quantidade = models.IntegerField(blank=True, null=True)
    observacoes = models.TextField(max_length=150, blank=True, null=True)
    safra = models.ForeignKey(Safra, on_delete=models.CASCADE, null=True, default=None)

class UpfIntegrante(BaseModel):
    nome = models.CharField(max_length=150, blank=True, null=True)
    data_nascimento = models.DateField(blank=True, null=True)
    main_integrante = models.BooleanField(blank=True, null=True)
    parentesco = models.CharField(max_length=150, blank=True, null=True)
    genero = models.CharField(max_length=150, blank=True, null=True)
    safra = models.ForeignKey(Safra, on_delete=models.CASCADE, null=True, default=None)

    def __str__(self):
        return self.nome

class TrabalhoUpf(BaseModel):
    data = models.DateField(blank=True, null=True)
    dias = models.FloatField(blank=True, null=True)
    remuneracao_desejada = models.FloatField(blank=True, null=True)
    observacoes = models.TextField(max_length=150, blank=True, null=True)
    safra = models.ForeignKey(Safra, on_delete=models.CASCADE, null=True, default=None)
    integrantes = models.ManyToManyField(UpfIntegrante, through='TrabalhoUpfIntegrante')

class TrabalhoUpfIntegrante(BaseModel):
    trabalho_upf = models.ForeignKey(TrabalhoUpf, on_delete=models.CASCADE, null=True, default=None)
    upf_integrante = models.ForeignKey(UpfIntegrante, on_delete=models.CASCADE, null=True, default=None)

class TipoEquipamento(models.Model):
    nome = models.CharField(max_length=150, blank=True, null=True)

class Equipamento(BaseModel):
    tipo = models.CharField(max_length=150, blank=True, null=True)
    marca_e_modelo = models.CharField(max_length=150, blank=True, null=True)
    ano_fabricacao = models.IntegerField(blank=True, null=True)
    ano_compra = models.IntegerField(blank=True, null=True)
    quantidade = models.IntegerField(blank=True, null=True)
    valor_compra = models.FloatField(blank=True, null=True)
    durabilidade_estimada_anos = models.IntegerField(blank=True, null=True)
    usado = models.BooleanField(blank=True, null=True)
    safra = models.ForeignKey(Safra, on_delete=models.CASCADE, null=True, default=None)
    # tipo_equipamento = models.ForeignKey(TipoEquipamento, on_delete=models.CASCADE, null=True, default=None)

class VersaoBancoDados(BaseModel):
    versao = models.CharField(max_length=150, blank=True, null=True)

class Estado(models.Model):
    uf = models.CharField(max_length=2, blank=True, null=True)
    nome = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        ordering = ('nome',)

    def __str__(self):
        return self.nome

class Municipio(models.Model):
    estado = models.ForeignKey(Estado, models.PROTECT)
    nome = models.CharField(max_length=150, blank=True, null=True)
    latitude = models.DecimalField(blank=True, null=True, max_digits=7, decimal_places=5)
    longitude = models.DecimalField(blank=True, null=True, max_digits=8, decimal_places=5)
    capital = models.BooleanField(blank=True, null=True)

    class Meta:
        ordering = ('nome',)

    def __str__(self):
        return f"{self.nome} / {self.estado.uf}"
