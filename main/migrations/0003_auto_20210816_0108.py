# Generated by Django 3.1.7 on 2021-08-16 01:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20210810_1600'),
    ]

    operations = [
        migrations.AlterField(
            model_name='despesa',
            name='data',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='producao',
            name='data',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='safra',
            name='data_final',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='safra',
            name='data_inicio',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='trabalhoupf',
            name='data',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='upf',
            name='data_nascimento_usuario',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='upfintegrante',
            name='data_nascimento',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='venda',
            name='data',
            field=models.DateField(blank=True, null=True),
        ),
    ]
