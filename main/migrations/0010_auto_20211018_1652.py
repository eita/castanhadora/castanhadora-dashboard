# Generated by Django 3.1.7 on 2021-10-18 16:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20211013_1640'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='despesa',
            name='categoria_despesa',
        ),
        migrations.RemoveField(
            model_name='equipamento',
            name='tipo_equipamento',
        ),
    ]
