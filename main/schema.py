import graphene
from graphene_django import DjangoObjectType

from main import models
from django.utils.timezone import make_aware, get_current_timezone
from django.db import transaction
from django.db.utils import IntegrityError
import traceback
    

class DespesaInput(graphene.InputObjectType):
    id = graphene.Int()
    tipo = graphene.String()
    valor = graphene.Float()
    data = graphene.Date()
    observacoes = graphene.String()
    aviamento = graphene.Boolean()
    diaria_dias = graphene.Float()
    diaria_quantidade_diaristas = graphene.Int()
    diaria_valor_diaria = graphene.Float()
    safra_id = graphene.Int()

class CategoriaDespesaInput(graphene.InputObjectType):
    id = graphene.Int()
    nome = graphene.String()

class VendaInput(graphene.InputObjectType):
    id = graphene.Int()
    quantidade = graphene.Int()
    valor_unitario = graphene.Float()
    data = graphene.Date()
    observacoes = graphene.String()
    safra_id = graphene.Int()
    comprador_id = graphene.Int()
    comprador = graphene.String()
    quitacao_aviamento = graphene.Boolean()

class ProducaoInput(graphene.InputObjectType):
    id = graphene.Int()
    data = graphene.Date()
    quantidade = graphene.Int()
    safra_id = graphene.Int()
    observacoes = graphene.String()

class TrabalhoUpfInput(graphene.InputObjectType):
    id = graphene.Int()
    data = graphene.Date()
    dias = graphene.Float()
    remuneracao_desejada = graphene.Float()
    observacoes = graphene.String()
    safra_id = graphene.Int()

class UpfIntegranteInput(graphene.InputObjectType):
    id = graphene.Int()
    nome = graphene.String()
    data_nascimento = graphene.Date()
    safra_id = graphene.Int()
    main_integrante = graphene.Boolean()
    parentesco = graphene.String()
    genero = graphene.String()

class TrabalhoUpfIntegranteInput(graphene.InputObjectType):
    id = graphene.Int()
    trabalho_upf_id = graphene.Int()
    upf_integrante_id = graphene.Int()

class EquipamentoInput(graphene.InputObjectType):
    id = graphene.Int()
    tipo = graphene.String()
    marca_e_modelo = graphene.String()
    ano_fabricacao = graphene.Int()
    ano_compra = graphene.Int()
    quantidade = graphene.Int()
    valor_compra = graphene.Float()
    durabilidade_estimada_anos = graphene.Int()
    usado = graphene.Boolean()
    safra_id = graphene.Int()
    
class TipoEquipamentoInput(graphene.InputObjectType):
    id = graphene.Int()
    nome = graphene.String()

class VersaoBancoDadosInput(graphene.InputObjectType):
    id = graphene.Int()
    versao = graphene.String()

class SafraInput(graphene.InputObjectType):
    id = graphene.Int()
    nome = graphene.String()
    data_inicio = graphene.Date()
    data_final = graphene.Date()
    ativa = graphene.Boolean()
    remuneracao_desejada_por_dia = graphene.Float()
    castanhal_tamanho = graphene.Float()
    castanhal_numero_castanheiras = graphene.Int()
    castanhal_numero_familias = graphene.Int()
    upf_id = graphene.Int()
    municipio_id = graphene.Int()
    uf_id = graphene.Int()
    simulacao = graphene.Boolean()
    localizacao_lat = graphene.Float()
    localizacao_long = graphene.Float()
    despesa_list = graphene.List(DespesaInput)
    venda_list = graphene.List(VendaInput)
    producao_list = graphene.List(ProducaoInput)
    trabalho_upf_list = graphene.List(TrabalhoUpfInput)
    upf_integrante_list = graphene.List(UpfIntegranteInput)
    trabalho_upf_integrante_list = graphene.List(TrabalhoUpfIntegranteInput)
    equipamento_list = graphene.List(EquipamentoInput)

class UpfInput(graphene.InputObjectType):
    id = graphene.Int()
    safra = graphene.Field(SafraInput)
    nome_comunidade = graphene.String()
    nome_usuario = graphene.String()
    data_nascimento_usuario = graphene.Date()
    genero_usuario = graphene.String()
    unidade_area = graphene.String()
    unidade_valor = graphene.String()
    cooperativa = graphene.String()
    mobile_key = graphene.String()

class Upf(DjangoObjectType):
    class Meta:
        model = models.Upf

class UpfMutation(graphene.Mutation):
    mobile_key = graphene.String()
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    class Arguments:
        upf_data = UpfInput(required=True)

    def mutate(self, info, upf_data=None):
        with transaction.atomic():
            mobile_key = None
            errors = []
            success = False

            try:
                # gera senha temporária
                if upf_data.mobile_key == "":
                    senha_aleatoria = models.Upf.gera_senha_aleatoria()
                else:
                    senha_aleatoria = upf_data.mobile_key

                safra_data = upf_data.safra

                upf, upf_created = models.Upf.objects.update_or_create(mobile_model_id=upf_data.id, mobile_key=senha_aleatoria,
                    defaults={'nome_comunidade':upf_data.nome_comunidade, 'nome_usuario':upf_data.nome_usuario, 'data_nascimento_usuario':upf_data.data_nascimento_usuario, 'genero_usuario':upf_data.genero_usuario, 'unidade_area':upf_data.unidade_area, 'unidade_valor':upf_data.unidade_valor, 'cooperativa':upf_data.cooperativa})
                
                safra, safra_created = models.Safra.objects.update_or_create(mobile_model_id=safra_data.id, mobile_key=senha_aleatoria,
                    defaults={'nome':safra_data.nome, 'data_inicio':safra_data.data_inicio, 'data_final':safra_data.data_final, 'ativa':safra_data.ativa, 'remuneracao_desejada_por_dia':safra_data.remuneracao_desejada_por_dia, 'castanhal_tamanho':safra_data.castanhal_tamanho, 'castanhal_numero_castanheiras':safra_data.castanhal_numero_castanheiras, 'castanhal_numero_familias':safra_data.castanhal_numero_familias, 'upf':upf, 'municipio_id':safra_data.municipio_id, 'uf_id':safra_data.uf_id, 'simulacao': safra_data.simulacao, 'localizacao_lat': safra_data.localizacao_lat, 'localizacao_long': safra_data.localizacao_long})

                new_despesas_id = []
                for new_despesa in safra_data.despesa_list:
                    despesa, _ = models.Despesa.objects.update_or_create(mobile_model_id=new_despesa.id, mobile_key=senha_aleatoria,
                        defaults={'tipo':new_despesa.tipo, 'valor':new_despesa.valor, 'data':new_despesa.data, 'observacoes':new_despesa.observacoes, 'aviamento':new_despesa.aviamento, 'diaria_dias':new_despesa.diaria_dias, 'diaria_quantidade_diaristas':new_despesa.diaria_quantidade_diaristas, 'diaria_valor_diaria':new_despesa.diaria_valor_diaria, 'safra':safra})
                    
                    new_despesas_id.append(despesa.id)
                models.Despesa.objects.filter(mobile_key=senha_aleatoria, safra=safra).exclude(id__in=new_despesas_id).delete()
                
                new_vendas_id = []
                for new_venda in safra_data.venda_list:
                    venda, _ = models.Venda.objects.update_or_create(mobile_model_id=new_venda.id, mobile_key=senha_aleatoria,
                        defaults={'quantidade':new_venda.quantidade, 'valor_unitario':new_venda.valor_unitario, 'data':new_venda.data, 'observacoes':new_venda.observacoes, 'safra':safra, 'comprador_id':new_venda.comprador_id, 'comprador':new_venda.comprador, 'quitacao_aviamento':new_venda.quitacao_aviamento})
                    new_vendas_id.append(venda.id)
                models.Venda.objects.filter(mobile_key=senha_aleatoria, safra=safra).exclude(id__in=new_vendas_id).delete()

                new_producoes_id = []
                for new_producao in safra_data.producao_list:
                    producao, _ = models.Producao.objects.update_or_create(mobile_model_id=new_producao.id, mobile_key=senha_aleatoria,
                        defaults={'data':new_producao.data, 'quantidade':new_producao.quantidade, 'safra':safra, 'observacoes':new_producao.observacoes})
                    new_producoes_id.append(producao.id)
                models.Producao.objects.filter(mobile_key=senha_aleatoria, safra=safra).exclude(id__in=new_producoes_id).delete()

                new_trabalhos_id = []
                for new_trabalho_upf in safra_data.trabalho_upf_list:
                    trabalho_upf, _ = models.TrabalhoUpf.objects.update_or_create(mobile_model_id=new_trabalho_upf.id, mobile_key=senha_aleatoria,
                        defaults={'data':new_trabalho_upf.data, 'dias':new_trabalho_upf.dias, 'remuneracao_desejada':new_trabalho_upf.remuneracao_desejada, 'observacoes':new_trabalho_upf.observacoes, 'safra':safra})
                    
                    models.TrabalhoUpfIntegrante.objects.filter(trabalho_upf__id=trabalho_upf.id).delete()
                    new_trabalhos_id.append(trabalho_upf.id)
                models.TrabalhoUpf.objects.filter(mobile_key=senha_aleatoria, safra=safra).exclude(id__in=new_trabalhos_id).delete()

                new_upf_integrantes_id = []
                for new_upf_integrante in safra_data.upf_integrante_list:
                    upf_integrante, _ = models.UpfIntegrante.objects.update_or_create(mobile_model_id=new_upf_integrante.id, mobile_key=senha_aleatoria,
                        defaults={'nome':new_upf_integrante.nome, 'data_nascimento':new_upf_integrante.data_nascimento, 'safra':safra, 'main_integrante':new_upf_integrante.main_integrante, 'parentesco':new_upf_integrante.parentesco, 'genero':new_upf_integrante.genero})
                    new_upf_integrantes_id.append(upf_integrante.id)
                models.UpfIntegrante.objects.filter(mobile_key=senha_aleatoria, safra=safra).exclude(id__in=new_upf_integrantes_id).delete()

                for new_trabalho_upf_integrante in safra_data.trabalho_upf_integrante_list:
                    trabalho_upf = models.TrabalhoUpf.objects.get(mobile_model_id=new_trabalho_upf_integrante.trabalho_upf_id, mobile_key=senha_aleatoria)
                    upf_integrante = models.UpfIntegrante.objects.get(mobile_model_id=new_trabalho_upf_integrante.upf_integrante_id, mobile_key=senha_aleatoria)
                    trabalho_upf_integrante = models.TrabalhoUpfIntegrante.objects.create(mobile_model_id=new_trabalho_upf_integrante.id, mobile_key=senha_aleatoria,
                        trabalho_upf=trabalho_upf, upf_integrante=upf_integrante)

                new_equipamentos_id = []
                for new_equipamento in safra_data.equipamento_list:
                    equipamento, _ = models.Equipamento.objects.update_or_create(mobile_model_id=new_equipamento.id, mobile_key=senha_aleatoria,
                        defaults={'tipo':new_equipamento.tipo, 'marca_e_modelo':new_equipamento.marca_e_modelo, 'ano_fabricacao':new_equipamento.ano_fabricacao, 'ano_compra':new_equipamento.ano_compra, 'quantidade':new_equipamento.quantidade, 'valor_compra':new_equipamento.valor_compra, 'durabilidade_estimada_anos':new_equipamento.durabilidade_estimada_anos, 'usado':new_equipamento.usado, 'safra':safra})
                    new_equipamentos_id.append(equipamento.id)
                models.Equipamento.objects.filter(mobile_key=senha_aleatoria, safra=safra).exclude(id__in=new_equipamentos_id).delete()


            except IntegrityError as exp:
                errors = ["username", "Dispositivo já cadastrado"]

            except Exception as exp:
                errors = ["error", traceback.format_exc()]

            if len(errors) == 0 and upf:
                success = True

        return UpfMutation(mobile_key=senha_aleatoria, success=success, errors=errors)

class Query(graphene.ObjectType):
    upfs = graphene.List(Upf)

    def resolve_upfs(self, info):
        return models.Upf.objects.all()

class Mutation(graphene.ObjectType):
    create_upf = UpfMutation.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
